/**
 * Created by Amulya on 7/11/2016.
 */

var Sequelize = require ( "sequelize");
var config = require('./config');

//create a connection with DB

var sequelize = new Sequelize(
    config.mysql.database,
    config.mysql.username,
    config.mysql.password,
    {
        host: config.mysql.host,
        dialect: 'mysql',
        define: {
            timestamps: false
        },
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    });//end of sequelize


    var groceryListModal = require('./model/groceryListDB')(sequelize,Sequelize);


    sequelize.sync({/*force: true*/})
    .then(function(){
        console.log("Databse in Sync now");
    }).catch (function(err){
        console.log("Database Sync failed");
    });

 module.exports= {
     GroceryList: groceryListModal
};