var express= require('express');
var bodyParser=require('body-parser');

var app = express();
//app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(express.static(__dirname + "/../client/"));
app.use("/bower_components", express.static(__dirname + "/../client/bower_components"));

//call the routing logic here.
var routes = require('./routes');
routes.set(app);

app.use(function(req, res, next) {
    console.log("URL does not match. Resource not found -  404 error");
    res.type("text/plain");
    res.status(404);
    res.end("Page not found");
});

app.set("port", process.argv[2] || process.env.APP_PORT || 3000);
app.listen(app.get("port"), function() {
    console.info("Web Application started on port %d", app.get("port"));
});