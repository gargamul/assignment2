/**
 * Created by Amulya on 7/11/2016.
 */
'use strict';

// NODE_ENV refers to production, the fallback is "development"
var ENV = process.env.NODE_ENV || "development";

module.exports = require ('./' + ENV +'.js');