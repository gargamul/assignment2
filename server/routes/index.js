/**
 * Created by Amulya on 7/11/2016.
 */

var GroceryList = require('./../database.js').GroceryList;

module.exports.set = function (app){
    console.log("Hello from the routes file");

    //to initialise our list firsthand
    app.post('/api/groceries/',function(req,res){
        console.log("going to find all in db");
        GroceryList.findAll({ limit : 20 , order : ['name'] })
            .then(function(response){
                console.log("This is init result successful"+response);
                res.status(200).json(response);
            })
            .catch(function(err){
                console.log("Error trying fetch default grocery list "+ err);
                res.status(400).send(JSON.stringify("Database error"));
            });
    });

    //get by name
    app.get('/api/groceries/name/:name',function(req,res){
        console.log("going to find all by name in db" + req.params.name);
        GroceryList.findAll({ where : {name : req.params.name }, limit : 20 , order : ['brand'] })
            .then(function(response){
                console.log("This is init result successful"+JSON.stringify(response));
                res.status(200).json(response);
            })
            .catch(function(err){
                console.log("Error trying fetch default grocery list "+ err);
                res.status(400).send(JSON.stringify("Database error"));
            });
    });

    //get by brand
    app.get('/api/groceries/brand/:brand',function(req,res){
        console.log("going to find all by brand in db" + req.params.brand);
        GroceryList.findAll({ where : {brand : req.params.brand}, limit : 20 , order : ['name'] })
            .then(function(response){
                console.log("This is init result successful"+JSON.stringify(response.body));
                res.status(200).json(response);
            })
            .catch(function(err){
                console.log("Error trying fetch default grocery list "+ err);
                res.status(400).send(JSON.stringify("Database error"));
            });
    });

    //get by id
    app.get('/api/groceries/id/:id',function(req,res){
        console.log("going to find all by brand in db" + req.params.id);
        GroceryList.findAll({ where : {id : req.params.id} })
            .then(function(response){
                console.log("This result successful"+JSON.stringify(response.body));
                res.status(200).json(response);
            })
            .catch(function(err){
                console.log("Error trying fetch default grocery list "+ err);
                res.status(400).send(JSON.stringify("Database error"));
            });
    });

    //put the value
    app.put('/api/groceries/:ID', function (req, res) {
        GroceryList.find({ where: { id: req.params.ID } })
            .then(function (grocery) {
                // Check if record exists in db
                console.log("This is the grocery: " + grocery);
                if (grocery) {
                    grocery.updateAttributes({
                        name: req.body.name,
                        upc12: req.body.upc12,
                        brand: req.body.brand
                    }).then(function (response) {
                        console.log("Update successful.");
                        res.status(200).json(response);
                    }).catch(function(err){
                        console.log("Error trying to write to DB "+ err);
                        res.status(400).send(JSON.stringify("Failed to set the changes in task in the database."));
                    });
                }
            });
    });//end of put




};

