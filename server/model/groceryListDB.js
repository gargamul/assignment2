/**
 * Created by Amulya on 8/11/2016.
 */

module.exports = function (sequelize,Sequelize){
    console.log("Connecting to our database from inside grocerylistDB");
    return sequelize.define('grocery_list', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        upc12: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        brand: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        name: {
            type: Sequelize.TEXT,
            allowNull: false
        }

    }, {
        tableName: 'grocery_list',
        timestamp : false
    });
};

