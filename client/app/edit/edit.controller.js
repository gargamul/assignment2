/**
 * Created by Amulya on 8/11/2016.
 */

(function() {
    angular.module("myApp")
        .controller("EditCtrl",EditCtrl);

    EditCtrl.$inject=["$http","$stateParams","$state"];
    function EditCtrl($http,$stateParams,$state){

        var vm=this;
        console.log("Printing out id from edit :" +JSON.stringify($stateParams));


        search();
        vm.save=save;
        vm.back=back;
        vm.editable=false;


        //perform a get
        function search(){

            $http.get('/api/groceries/id/'+$stateParams.Id)
                .then(function(groceries){
                    vm.grocery=groceries.data[0];
                    vm.editable=true;
                    console.log("got this from DB in edit"+JSON.stringify(groceries.data));
                    console.log("What we have in the edit object"+JSON.stringify(vm.grocery));
                })
                .catch(function(err){
                    console.log("Failed to find this in the DB");
                });
        }
        //perform an edit
        function save(){
            if(vm.editable==true){
                $http.put('/api/groceries/'+vm.grocery.id,vm.grocery)
                    . then (function (response){
                    console.log("Successfully managed to save the values");

                })  .catch (function(err){
                    console.log("Failed to save data on DB");

                });
            }

        }//end of edit

        function back(){
            console.log ("Going back to the front page");
            $state.go("A");
        }

    }//close of controller
})();

