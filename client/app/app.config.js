(function () {
    angular
        .module("myApp")
        .config(AppConfig);

    AppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function AppConfig($stateProvider,$urlRouterProvider){

        console.log("Hi i am inside the app.config");

        $stateProvider
            .state('A',{
                url : '/A',
                templateUrl :'./app/search/search.html',
                controller : 'SearchCtrl',
                controllerAs : 'ctrl'
            })
            .state('C1',{
                url : '/C1/:Id',
                templateUrl :'./app/edit/edit.html',
                controller : 'EditCtrl',
                controllerAs : 'ctrl'
            });

        $urlRouterProvider.otherwise("/A");


    }
})();