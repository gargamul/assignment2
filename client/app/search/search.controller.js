/**
 * Created by Amulya on 8/11/2016.
 */
(function () {
    angular
        .module("myApp")
        .controller("SearchCtrl", ["$http","$state",  SearchCtrl]);

    function SearchCtrl($http, $state) {
        var vm = this;

        vm.list = {}; //might need to initialise fields. Lets see
        vm.searchValue=""; // the search input
        vm.searchByBrand=searchByBrand;
        vm.searchByName=searchByName;
        vm.edit=edit;

        init();

        function init(){
            console.log("Going to initalise the list");
            $http.post('/api/groceries/')
                .then(function(groceryList){
                    vm.list=groceryList.data;
                })
                .catch (function(err){
                    console.log("Got error in initalising"+ err);
                })
        }

        function searchByBrand(){
            console.log("From UI I m going to search for brand");
            $http.get('/api/groceries/brand/'+vm.searchValue)
                .then (function(groceryList){
                    vm.list=groceryList.data;
                    console.log("The brand list"+ groceryList.data);
                })
                .catch (function (err) {
                    console.log("failed to get groceries for brand "+ vm.searchValue+" : "+err);
                })
        }

        function searchByName(){
            console.log("Going to search by name" + vm.searchValue);
            $http.get('/api/groceries/name/'+vm.searchValue)
                .then (function(groceryList){
                    vm.list=groceryList.data;
                    console.log("Successfully queried the DB" + groceryList.data);
                })
                .catch (function (err) {
                    console.log("failed to get groceries for name "+ vm.searchValue+" : "+err);
                })
        }

        function edit(idEdit){
            console.log("Edit this groceryid "+idEdit);
            $state.go("C1", {Id:idEdit});
            //call the init if init not auto called
        }

    } // close of controller

})();